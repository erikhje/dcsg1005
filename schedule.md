[[_TOC_]]

# About the Course

This course is [DCSG1005 Infrastructure: Secure Core
Services](https://www.ntnu.no/studier/emner/DCSG1005). Teacher is [Erik
Hjelmås](https://www.ntnu.no/ansatte/erik.hjelmas). Teaching assistants are
Ole Kristian Morud (ole.k.morud@ntnu.no), Deimanté Griguolaité (deimantg@stud.ntnu.no) and Mikael
Lauvlid Finsrud (mikaellf@stud.ntnu.no)

## Expected background (from fall courses)

* Linux command line
* Basics of computers, network and virtual machines (VMs)
* Some programming skills
* Some security mindset

## Course setup

**This course is based on the flipped-classroom concept, meaning you have
to watch the video lectures BEFORE coming to class on Wednesdays. All lectures are
published in Blackboard and [also on
YouTube](https://www.youtube.com/playlist?list=PLmiI5VoyQBWMFY28WyGFvmlxQWxorwQcd).**
On Wednesdays, we will work on exercises together using Menti, and you can ask
questions as we go along, and teacher will give mini-lectures as needed to
clarify any topic you find difficult to understand.

| Time&Place | Happening |
| ---- | ------------- |
| Tuesdays 0815-1000 week 7, 10, 13 | Team-Based Learning |
| Wednesdays 0815-1000 K102 | Interactive lecture/exercises with Erik |
| Wednesdays 1015-1200 A255 | Exercises with teaching assistants |
| Thursdays 1415-1600 A255 | Exercises with teaching assistants |

([See TP](https://tp.uio.no/ntnu/timeplan/?id=DCSG1005&type=course&sort=form&week=2&weekTo=18&ar=2025) for official schedule of course).

We try to stay practical in the course so always bring your laptop.

1. Ask questions in the classroom on Wednesdays and Thursdays. During
   interactive lectures, we will use Menti where you can enter your
   questions, and during exercises you can raise your hand, and we
   will help you.
1. Contact teaching assistants or Erik in Teams chat or Discord.

## Readings

* See links in "Topic and Readings" in the [Weekly Schedule](https://gitlab.com/erikhje/dcsg1005/-/blob/master/schedule.md#weekly-schedule).
* Compendium with all lecture slides, lecture notes, review questions and problems and lab exercises
  ([PDF](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) for
  reading,
  [Markdown](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.md) for
  easy copy-and-paste). The compendium is also on sale in Akademika if you
  would like a paper version.

## Mandatory exercises

* You have to work on the exercises in the compendium every week.
  Approximately every second or third week (see schedule below, five
  deadlines in total), you have to demonstrate that you have completed a
  few selected exercises to the teaching assistants. **You have to
  attend and successfully demonstrate your work at least four out of the
  five demo sessions.**
* We will also have a Team-Based Learning (TBL) sessions in week 7, 10
  and 13. They are not mandatory, but very useful for learning, **strongly recommended**.

<!--
---------
* There are three mandatory exercises (multiple choice tests).
-->
* **The exam will have questions based on the weekly "Review questions and
  problems" and "Lab tutorials"**, so it is strongly encouraged to be active and
  do all the exercises every week.
<!--
* The project counts 40% of your grade and will be announced on Feb 27th with
  hand-in on March 20st. You should form groups of two or three (preferably three) for this
  project (create the groups yourselves). Individual hand-ins is possible but not recommended (since use of git and coding is a much better learning experience in a group).

## How to get a good grade

* Do all the "Lab tutorials" (lab tutorials are "easy" exercises where you read
  the text, and copy/paste commands) and "Review questions and problems" in [the
  compendia](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf)
  (pay particular attention to the exercises marked as "Key problems", they
  represent typical advanced questions you will get on the exam and/or they are especially important for your learning).
* Read the compendia and any additional material listed in the Weekly schedule.
* Show up for all sessions in the timeplan.
* Watch all the lectures.
-->

## Weekly schedule

Note: together with the weekly topic and readings, there is a list of keywords.
The purpose of these keywords is for you to get a quick overview of what we are
covering each week, and also for you to quickly review that you are familiar
with the material we have covered. Also note: **The slides I show you when I
give lectures are the figures in the compendium. You do not have to ask me to
publish my slides, they are all in the compendium as the box-framed figures**.

| Week |  Topic and Readings | Additional info |
| ---  | ---                 | ---             |
| 2    | **Introduction: NSM Grunnprinsipper, Cloud Computing and OpenStack ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 1)** | |
| 3    | **Windows Server ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 1)**<br/>public vs private cloud, IaaS-PaaS-SaaS, dynamic, self-service, pay-as-you-go, privacy and asset management, OpenStack components, what is OpenStack, orchestration, process and thread, what is a service, kinds of services, svchost, user vs system account, registry, WMI, roles and features, Sysinternals, Chocolatey, cmd vs PowerShell | **1.6-7.(i) Demo deadline Jan 16th** (note: 1.6-7.(i) means compendium chapter 1.6, item number 7, subitem (i)) |
| 4-5    | **PowerShell ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 2)**<br/>versions, ExecutionPolicy, cmdlet, parameter, alias, help, Get-Alias, Get-ChildItem, Get-Help, Get-Service, Select-String, drive, profile, variable, namespace, environment, Get-PSDrive, Get-Content, Write-Output, object/property/method, Resolve-DnsName, pipeline, Select-Object, Get-Process, Where-Object, $_ , Format-List, Format-Table, Out-File, Export-Csv, ForEach-Object, Sort-Object, Get-Date, Measure-Object, Compare-Object, Get-LocalUser, if-tests, modules, Find-Module, Install-Module, Get-WindowsUpdate, here-string, scripts, call operator, Invoke-ScriptAnalyzer          | **2.3-7, 2.3-8 and 2.3-11 Demo deadline Jan 30th** |
| 6    | **Storage, Backup and Restore ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 3)**<br/>file- vs block-level, file metadata vs data, disk/partition/volume, ACL/ACE, NSM chp 2.9, why backup?, ransomware, what to backup?, ignore files, B-MAC, Restic, Immutable, full/partial/differential/incremental, dump level, 3-2-1 backup, permissions/privileges, NSM measure 2.6.5, "files in use" and VSS, compression/encryption/deduplication, verify, restore |  |
| 7    | **Active Directory: DNS, LDAP and Kerberos ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 5)**<br/>RFC, TLD, ICANN, NORID, resolver/cache vs authoritative server, Bind, dig, nslookup, Resolve-DnsName, domain vs zone, FQDN, LDH-rule, resource records (RRs), SOA-NS-MX-A-CNAME-PTR-SRV, root-servers, name-type-value-TTL, recursive vs iterative, hosts-file, Get-DnsClientServerAddress, Get-DnsClientCache, DHCP, dynamic DNS, attributes/properties, read-write ratio, distinguished name (DN), relative distinguished name (RDN), DC, OU, CN, Get-ADObject, ldapsearch, schema, objectClass, atomic, FEIDE, centralized authentication, single-sign on (SSO), port 389, StartTLS, Key Distribution Center (KDC), authentication server (AS), ticket-granting server ticket (TGS ticket), service principal name (SPN), ticket-granting-ticket (TGT), golden ticket, silver ticket, kerberoasting | Team-Based Learning |
| 8    | **Active Directory: Design and Implementation ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 6)**<br/>Windows domain, domain controller, forest-tree-domain, replication, perimeter security vs zero trust, identity and access management, asset management, policy-based configuration management, organizational unit (OU), OU-container-group, RootDSE, users, computers, computer accounts (machine accounts), group policy objects (GPOs), site, New-ADOrganizationalUnit, Get-ADComputer, Move-ADComputer, Import-Csv, New-ADuser, Get-ADUser, AGDLP, distribution vs security groups, universal vs global vs domain local groups, SID, Builtin, New-ADGroup, Add-ADGroupMember, Get-ADGroupMember, Add-LocalGroupMember, Get-LocalGroupMember |  |
| 9    | **Remoting, Config Management and Group Policy ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 7)**<br/>push vs pull, ports 22, 135, 137, 139, 443, 445, 5985, 5986, WSMan (WinRM), Enter-PSSession, Invoke-Command, New-PSSession, Get-PSSession, Remove-PSSession, SMB, file share, NETLOGON, SYSVOL, UNC, Get-SmbShare, Get-SmbShareAccess, gpsvc, gpupdate, gpresult, Invoke-GPUpdate, Get-GPOReport, local group policy, processing order, software settings, windows settings, administrative templates, notconfigured-enabled-disabled, Get-GPO, New-GPLink, settings vs preferences, security baseline, gpedit-gpme-gpmc, psexec, Intune, Endpoint Configuration Manager  | **6.5-1, 6.4-1.(a) Demo deadline Feb 27th** |
| 10    | **Software Package Management ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 8)**<br/>executables, libraries, licenses, Get-WindowsFeature, Get-WindowsCapability, Win32_Product, Get-Package, Get-HotFix, msi-msix-msu-appx, choco-scoop-appget-winget-ninite, npm-pypi-ppm-rubygems, installer vs package manager, patch tuesday, one-some-many, updates vs fresh install, supply chain attack, typesquatting, account hijacking, social engineering, replay attack, freeze attack, metadata manipulation attack, endless data attack, CVE, NVD, CVSS, Mitre Att&ck, WSUS, PackageManagement/OneGet  | Team-Based Learning |
| 11-12   | **Logging and Monitoring ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 9)**<br/>periodic/accumulating counters, counterset, counter, instance, path, single-instance/multi-instance counters, Get-Counter, Get-CimInstance, raw vs secondary value, CookedValue, Windows Admin Center, log events, event provider, event log, event type, EventID, log mode, timestamp, hostname, process name/source, message, Application, Security, System, Get-WinEvent, log levels (Verbose, Informational, Warning, Error, Critical, LogAlways), regex, wildcards, special characters, Select-String, anchoring, grouping, modifiers/quantifiers/repetition operators, matches array, message field structure | **7.6-5, 9.7-13 Demo deadline March 20nd** |
| 13-14  | **Security: Attacks and Defenses ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 10 and 11)**<br/>cyber kill chain, Mitre Att&ck Matrix, adversary group, APT, TTPs (tactics, techniques, procedures), sub-technique, mitigation, data source, detection, CAPEC, Att&ck Navigator, Atomic red team, Invoke-AtomicTest, BloodHound, SharpHound, Mitre D3fend, harden, detect, isolate, deceive, evict, Defender | Team-Based Learning |
| 15   | **Infrastructure Orchestration ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 12)**<br/>repeatability and consistency, imperative vs declarative, domain-specific language, YAML and JSON, Heat, Cloudformation, ARM templates, Cloud Deployment Manager, Terraform, HCL, Heat version/parameter/resources/outputs, stack, configuration definition file, resources dependencies, conditions, iteration, boot scripts, nested stacks, openstack stack commands | **12.6-4 Demo deadline April 10th** |
| 17   | Repetition, Exam info |  |
| 20 | | Digital exam Inspera May 13th 09:00-12:00 | | |


<!--
| 6    | **Git, markdown and CI/CD ([compendium](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) chp 4)**<br/>config, init, workflow, file states, commit/versions, conventional commits, log, working areas, conflicts, basic-extended-GitLab/GitHub Markdown, DevOps, CI/CD, pipeline | Multiple choice test one & Team-Based Learning |
-->


<!--

Punchline sequence:
- PowerShell piping objects with methods and properties
- Storage, backup, ransomware
- git with sensible commits, shared repo, and basics CI/CD: "scripts, configs and docs as code"
- AD and DNS
- building an AD infrastructure
- policies and configs
- software pkg managment

project

- establish timeline of events from log
- find bugs in logs
- compare drive performance?
Windows Admin Center (perfmon)
carefully choose hardening and security testing tools


Alle oppgaver i kompendiet slik at kan gjøres alene hjemme, men antar live i grupper på tre-fire, VIKTIG: spm på eksamen som antar at dere har gjort alt det praktiske.

| Week Number and Day |  Topic and Readings | Exercises/Portfolio |
| ---  | ---                 | ---                 |
| 2 | **Introduction and tools** ([Compendia Chp 1](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf), [Introduksjon Windows Server](https://ntnu.blackboard.com/bbcswebdav/pid-1230441-dt-content-rid-33368006_1/xid-33368006_1), [Introduksjon PowerShell](https://ntnu.blackboard.com/bbcswebdav/pid-1230443-dt-content-rid-33368007_1/xid-33368007_1), [OpenStack](https://en.wikipedia.org/w/index.php?title=OpenStack&oldid=991678601), [Visual Studio Code](https://code.visualstudio.com/docs/getstarted/introvideos), [git](https://about.gitlab.com/images/press/git-cheat-sheet.pdf), [Markdown](https://docs.gitlab.com/ee/user/markdown.html))<br/>OpenStack GUI, ssh, git, vscode, powershell, public/private keys, key-based authentication, Heat-stack with OpenStack resources: network, subnet, router, gateway, interface, flavor, image, server, floating ip, security group, security group rules, volume | See lab exercises in Compendia Chp 1 |
| 3 | **Windows Server 2019 and PowerShell** ([Compendia Chp 2](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf), [GUI Windows Server](https://ntnu.blackboard.com/bbcswebdav/pid-1242343-dt-content-rid-33479523_1/xid-33479523_1), [PowerShell](./powershell.md))<br/>reboot, administrative tools (WinKey + X), task manager, (sconfig), search function, roles and features, Windows Admin Center, Networking: interface/ipaddress/netmask/gateway/DNS, updates, local users and groups, eventlog, Windows Defender, cmd, chocolatey, PowerShell: versions, ExecutionPolicy, cmdlet, parameter, alias, help, Get-Alias, Get-ChildItem, Get-Help, Get-Service, Select-String, drive, profile, variable, namespace, environment, Get-PSDrive, Get-Content, Write-Output, object/property/method, Resolve-DnsName, pipeline, Select-Object, Get-Process, Where-Object, $_, Format-List, Format-Table, Out-File, Export-Csv, ForEach-Object, Sort-Object, Get-Date, Measure-Object | See lab exercises in Compendia Chp 2 |
| 4 | **Windows Server 2019 and PowerShell** ([Compendia Chp 3](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf), [PowerShell](./powershell.md))<br/>Active Directory, scheduled tasks, PowerShell: modules, Find-Module, Install-Module, Get-WindowsUpdate, here-string, scripts, call operator, Invoke-ScriptAnalyzer, remoting, winrm, ssh, Enter-PSSession, New-PSSession, Get-PSSession, Invoke-Command, Get-NetTCPConnection, Test-WSMan, Get-SmbShare, New-SmbShare, Remove-SmbShare | **Portfolio 1 Release Monday**<br/>([Ransomware Protection](https://gitlab.com/erikhje/dcsg1005/-/blob/master/portfolio/portfolio1.md)) |
| 5  | **Data storage, backup and restore** |  |
| 6  | Lab week | **Portfolio 1 Hand-in Sunday** |
| 7  | **Directory Services: AD** (Blackboard, material from Tor Ivar) |  |
| 8  | [**Logging and Monitoring**](https://gitlab.com/erikhje/dcsg1005/-/tree/master/logging-monitoring) | **Portfolio 2 Release Tuesday**<br/>([Active Directory](https://ntnu.blackboard.com/webapps/blackboard/content/listContent.jsp?course_id=_22694_1&content_id=_979089_1&mode=reset)) |
| 9  | [**Configuration Management: Group Policy**](https://gitlab.com/erikhje/dcsg1005/-/tree/master/group-policy) | |
| 10 | Lab week | |
| 11 | Lab week | **Portfolio 2 Hand-in Friday** |
| 12 | [**Software Package Management**](https://gitlab.com/erikhje/dcsg1005/-/tree/master/pkgman)  | **Portfolio 3 Release Monday**<br/>([SW pkg mgmt](https://gitlab.com/erikhje/dcsg1005/-/blob/master/portfolio/portfolio3.md)) |
| 13 | Påskeferie |  |
| 14 | Lab week |  |
| 15 | **Public Cloud** (Blackboard, material from Tor Ivar) | **Portfolio 3 Hand-in Monday** |
| 16 | **Public Cloud** (Blackboard, material from Tor Ivar) |  |
| 17 | **Public Cloud** (Blackboard, material from Tor Ivar) | Portfolio 4 (Cloud)? |

(0. Hvordan dele inn i grupper? Mappeeval-oppgaver)
1. Heat stacks m disabled patch Tuesday i Win srv 2019?
LESE DE TO LEKSJONENE FRA Tor Ivar
2. Lab for å starte Heat stacks
3. To uker med PowerShell Menti og øvingsoppgaver
4. .

Start-Process powershell -ArgumentList "echo 5 M 15 | sconfig;sleep 10" -WindowStyle hidden

| Week | Learning outcome | Topic | Readings | Exercises/Portfolio |
| ---  | ---              | ---   | ---      | ---                 |
| 2 | K1,F1 | Cloud Computing (Horizon/OpenStack CLI) | [Cloud Computing](https://en.wikipedia.org/w/index.php?title=Cloud_computing&oldid=929942077), [OpenStack](https://en.wikipedia.org/w/index.php?title=OpenStack&oldid=932084017), Compendia Chp 1 | Do all "Lab tutorials" in Compendia Chp 1 (then do the "Review questions and Problems") |
| 3	| K1,K3,F1,F2 | Orchestration (OpenStack Heat) | [Complete idiot's introduction to yaml](https://github.com/Animosity/CraftIRC/wiki/Complete-idiot%27s-introduction-to-yaml), [An Introduction to OpenStack Heat](http://blog.scottlowe.org/2014/05/01/an-introduction-to-openstack-heat/), [Another Look at an OpenStack Heat Template](http://blog.scottlowe.org/2014/05/02/another-look-at-an-openstack-heat-template/), Compendia Chp 2 | Do all "Lab tutorials" in Compendia Chp 2 (then do the "Review questions and Problems") |
| 4	| F1,F2 | PowerShell | [PowerShell tutorial](https://gitlab.com/erikhje/dcsg1005/blob/master/powershell.md), Compendia Chp 3 | Do all "Lab tutorials" in Compendia Chp 3 (then do the "Review questions and Problems") |
| 5-6 | F1,F2,F5 | PowerShell environment | [PowerShell tutorial](https://gitlab.com/erikhje/dcsg1005/blob/master/powershell.md), Textbook Chp 1, 2, Compendia Chp 3 | Do all "Lab tutorials" in Compendia Chp 3 (then do the "Review questions and Problems") |
| 7-8 | F4 | DNS, AD (guest lecture AD w/Danny) | Textbook Chp 2, 3, [How DNS works](https://howdns.works/) (read all episodes incl bonus episode), Compendia Chp 4 | Frist Oblig1 (17.feb kl 16:00) Do all "Lab tutorials" in Compendia Chp 4 (then do the "Review questions and Problems") |
| 9 | K3,F4 | DNS, LDAP, Kerberos and AD, Group policy (guest lecture with Vebjørn 1015-1040) | [Group Policy](https://en.wikipedia.org/w/index.php?title=Group_Policy&oldid=942123014), Textbook Chp 3, Compendia Chp 4 | Frist Oblig2 (27.feb kl 16:00) Do all "Lab tutorials" in Compendia Chp 4 (then do the "Review questions and Problems") |
| 10 | K3,F4 | Storage and file services | Textbook Chp 4,5, Compendia Chp 5 | Do all "Lab tutorials" in Compendia Chp 5 (then do the "Review questions and Problems") |
| 11 | K3,F5 | Updates, Patching, Packages | Textbook Chp 6, Compendia Chp 6 | Do all "Lab tutorials" in Compendia Chp 6 (then do the "Review questions and Problems") |
| 12 | F2,F5 | Web Services and Certificates | Textbook Chp 9 | Do all "Lab tutorials" in Compendia Chp 7 (then do the "Review questions and Problems") |
| 13 | K2,F3,F5,F6  | Performance/Troubleshooting/Centralized logging/Monitoring | Textbook Chp 13,14 | Frist Oblig3 (25.mars kl 16:00) Do all "Lab tutorials" in Compendia Chp 8 (then do the "Review questions and Problems") | 
| 14 | | Security ([BlackHat Video](https://www.youtube.com/watch?v=FVe0Uaa65z0)) | [Active Directory Security Fundamentals](https://identityaccessdotmanagement.files.wordpress.com/2019/12/ad-security-fundamentals-1.pdf) | Do all "Lab tutorials" in Compendia Chp 9 (then do the "Review questions and Problems") |
| 16 | | Project | | |	 	 
| 17 | | Project | | | 
| 18 | | Project (+ Repetition, Exam info) | | Project deadline May 3rd | 
| 22 | | Digital home exam Inspera May 27th | | |
-->
